####################################################################
# Configuracion de Lista StopWord                                  #
#                                                                  #
# Diccionario                                                      #
#     STW = {'a' = [u'stwd1',u'stwd2',u'stwd3',...,u'stwdn']       #
#            ,'b' = [u'stwd1',u'stwd2',u'stwd3',...,u'stwdn']      #
#            ,...                                                  #
#            ,'z' = [u'stwd1',u'stwd2',u'stwd3',...,u'stwdn']}     #
#                                                                  #
# Indispensable sean unicodes                                      #
#                                                                  #
####################################################################

STW = {'a': ['a','able','about','above','abst','accordance','according','accordingly','across','act','actually','added','adj','affected','affecting','affects','after','afterwards','again','against','ah','all','almost','alone','along','already','also','although','always','am','among','amongst','an','and','announce','another','any','anybody','anyhow','anymore','anyone','anything','anyway','anyways','anywhere','apparently','approximately','are','aren','arent','arise','around','as','aside','ask','asking','at','auth','available','away','awfully']
       ,'b':['b','back','be','became','because','become','becomes','becoming','been','before','beforehand','begin','beginning','beginnings','begins','behind','being','believe','below','beside','besides','between','beyond','biol','both','brief','briefly','but','by']
       ,'c':['c','ca','came','can','cannot',"can't",'cause','causes','certain','certainly','co','com','come','comes','contain','containing','contains','could','couldnt']
       ,'d':['d','date','did',"didn't",'different','do','does',"doesn't",'doing','done',"don't",'down','downwards','due','during']
       ,'e':['e','each','ed','edu','effect','eg','eight','eighty','either','else','elsewhere','end','ending','enough','especially','et','etl','etc','even','ever','every','everybody','everyone','everything','everywhere','ex','except']
       ,'f':['f','far','few','ff','fifth','first','five','fix','followed','following','follows','for','former','formerly','forth','found','four','from','further','furthermore']
       ,'g':['g','gave','get','gets','getting','give','given','gives','giving','go','goes','gone','got','gotten']
       ,'h':['h','had','happens','hardly','has',"hasn't",'have',"haven't",'having','he','hed','hence','her','here','hereafter','hereby','herein','heres','hereupon','hers','herself','hes','hi','hid','him','himself','his','hither','home','how','howbeit','however','hundred']
       ,'i':['i','id','ie','if',"i'll",'im','immediate','immediately','importance','important','in','inc','indeed','index','information','instead','into','invention','inward','is',"isn't",'it','itd',"it'll",'its','itself',"i've",'j','just']
       ,'j':['j']
       ,'k':['k','keep','keeps','kept','kg','km','know','known','knows']
       ,'l':['l','largely','last','lately','later','latter','latterly','least','less','lest','let','lets','like','liked','likely','line','little',"'ll",'look','looking','looks','ltd']
       ,'m':['m','made','mainly','make','makes','many','may','maybe','me','mean','means','meantime','meanwhile','merely','mg','might','million','miss','ml','more','moreover','most','mostly','mr','mrs','much','mug','must','my','myself']
       ,'n':['n','na','name','namely','nay','nd','near','nearly','necessarily','necessary','need','needs','neither','never','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','nor','normally','nos','not','noted','nothing','now','nowhere']
       ,'o':['o','obtain','obtained','obviously','of','off','often','oh','ok','okay','old','omitted','on','once','one','ones','only','onto','or','ord','other','others','otherwise','ought','our','ours','ourselves','out','outside','over','overall','owing','own']
       ,'p':['p','page','pages','part','particular','particularly','past','per','perhaps','placed','please','plus','poorly','possible','possibly','potentially','pp','predominantly','present','previously','primarily','probably','promptly','proud','provides','put']
       ,'q':['q','que','quickly','quite','qv']
       ,'r':['r','ran','rather','rd','re','readily','really','recent','recently','ref','refs','regarding','regardless','regards','related','relatively','research','respectively','resulted','resulting','results','right','run']
       ,'s':['s','said','same','saw','say','saying','says','sec','section','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sent','seven','several','shall','she','shed',"she'll",'shes','should',"shouldn't",'show','showed','shown','showns','shows','significant','significantly','similar','similarly','since','six','slightly','so','some','somebody','somehow','someone','somethan','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specifically','specified','specify','specifying','still','stop','strongly','sub','substantially','successfully','such','sufficiently','suggest','sup','sure']
       ,'t':['t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that',"that'll",'thats',"that've",'the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','thered','therefore','therein',"there'll",'thereof','therere','theres','thereto','thereupon',"there've",'these','they','theyd',"they'll",'theyre',"they've",'think','this','those','thou','though','thoughh','thousand','throug','through','throughout','thru','thus','til','tip','to','together','too','took','toward','towards','tried','tries','truly','try','trying','ts','twice','two']
       ,'u':['u','un','under','unfortunately','unless','unlike','unlikely','until','unto','up','upon','ups','us','use','used','useful','usefully','usefulness','uses','using','usually']
       ,'v':['v','value','various',"'ve",'very','via','viz','vol','vols','vs']
       ,'w':['w','want','wants','was',"wasn't",'way','we','wed','welcome',"we'll",'went','were',"weren't","we've",'what','whatever',"what'll",'whats','when','whence','whenever','where','whereafter','whereas','whereby','wherein','wheres','whereupon','wherever','whether','which','while','whim','whither','who','whod','whoever','whole',"who'll",'whom','whomever','whos','whose','why','widely','will','willing','wish','with','within','without',"won't",'words','world','would',"wouldn't",'www']
       ,'x':['x']
       ,'y':['y','yes','yet','you','youd',"you'll",'your','youre','yours','yourself','yourselves',"you've"]
       ,'z':['z','zero']}

####################################################################
# Configuracion de Lista signos de puntuacion                      #
#                                                                  #
# Lista                                                            #
#     SGNPT = ['\!','"','#','\$',...,'%','&','/','=','\.]          #
#                                                                  #
# Indispensable sean unicodes                                      #
#                                                                  #
####################################################################

SGNPT =['&quot;','&apos;','&amp;','&lt;','&gt;','&nbsp;','&iexcl;','&cent;','&pound;',
        '&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;',
        '&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;',
        '&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;',
        '&frac34;','&iquest;','&times;','&divide;','&Agrave;','&Aacute;','&Acirc;','&Atilde;',
        '&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;',
        '&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;',
        '&Ocirc;','&Otilde;','&Ouml;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;',
        '&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;',
        '&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;',
        '&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;',
        '&otilde;','&ouml;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;',
        '&thorn;','&yuml;','\!','"','#','\$','%','&','/','=','\.',':',',',';','\{','\}',
        '\(','\)','\*','\+','\^','\[','\]','\|','-',"'",'\?',u'!','_',u'\ufeff','\u014','<','>','@','~','\`']

####################################################################
# Configuracion de Lista Formatos de Salida                        #
#                                                                  #
# Lista                                                            #
#     SGNPT = ['\n','\b','\r','\t','\d','\\','-','\\\n']           #
#                                                                  #
# Indispensable sean unicodes                                      #
#                                                                  #
####################################################################

FRTOUT = ['\n','\b','\r','\t','\d','\\','-','\\\n']

####################################################################
# Configuracion de expresion para eliminar etiqueta y sintaxis     #
# de lenguajes                                                     #
#                                                                  #
# Lista                                                            #
#     SGNPT = ['\n','\b','\r','\t','\d','\\','-','\\\n']           #
#                                                                  #
# Indispensable sean unicodes                                      #
#                                                                  #
####################################################################

REXTAG = u'<[^>]*>'

####################################################################
# Configuracion formatos de archivos planos archivos planos        #
# permitidos                                                       #
#                                                                  #
# Lista                                                            #
#     FMTFILE = ['.html']                                          #
#                                                                  #
# Indispensable sean unicodes                                      #
#                                                                  #
####################################################################

FMTFILE = ['.html']#,'.tex','.txt','.xml','.xhtml']

####################################################################
# Configuracion Archivo raiz                                       #
# permitidos                                                       #
#                                                                  #
# Lista                                                            #
#     PATH = '/home/isaias/Documents/TT_segundo_intento/root'      #
#                                                                  #
# Indispensable sean unicodes                                      #
#                                                                  #
####################################################################

#PATH = '/home/isaias/Documents/TT_segundo_intento/root'

#PATH = '/var/www/tt-app/public_html'

PATH = '/home/isaias/Documents/wiki'
