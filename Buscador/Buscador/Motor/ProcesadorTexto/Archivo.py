'''
Created on 30 Apr 2012

@author: isaias
'''
from re import sub as suprimirPalabra
from re import split as dividirTerminos
import removeSW
import listStemmer
import Config as cfg
#import PorterStemmer

class Archivo(object):
    ''' Maneja los archivos extraccion de constenido y primeros filtros 
    (html puntuacion simbologia)
    
    Casos de uso:   RFI-07
                    RF1-08
    
    '''
    def __init__(self,archivo):
        '''
        Constructor
        '''
        self.__formatosSalida = cfg.FRTOUT
        self.__signosPuntuacion = cfg.SGNPT
        self.__stopWords = cfg.STW
        self.__tagLenguajes = cfg.REXTAG
        
        try:
            self.__file = open(archivo,'r')
            self.__contenido = unicode(self.__file.read(),errors='ignore').lower()  
            self.__contenido = "".join(i for i in self.__contenido if ord(i)<128)

        except Exception, e:
            print "Error con el archivo "+ str(e)
            
           
    def __filtroContenido(self):
        ''' Remueve del contenido las etiquetas html, los formatos de salida y signos de puntuacion
        '''        
        contenido = suprimirPalabra(self.__tagLenguajes,' ',self.__contenido)
        for formato in self.__formatosSalida:
            contenido = suprimirPalabra(formato+'+',' ',contenido)
        for punto in self.__signosPuntuacion:
            contenido = suprimirPalabra(punto,' ',contenido)
        return contenido
            
    def getListTerminos(self):
        ''' Convierte el string en una lista donde cada elementos es un termino
        '''
        return dividirTerminos(' +',self.__filtroContenido())
    
    def __filtroStopWords(self,texto):
        ''' Borra todas las stopWords del Contenido prosesado
        '''
        textoStopWord = texto[:]
        for termino in texto:
            if not termino == "":
                if (termino in self.__stopWords[termino[:1]]):
                    textoStopWord.remove(termino)
        return textoStopWord  
        
    def getListTerminosStopWord(self,texto):
        ''' Regresa un arreglo solo con los terminos relevantes de un contenido
        '''
        return removeSW.remove(self.__stopWords,texto)
        #return  self.__filtroStopWords(texto)
         
    def Stemmer(self,texto):
        ''' Resive una lista de palabrar y regresa un lista solo con la raices (lemas) de cada una de la palabras
        '''
#        stem = PorterStemmer.PorterStemmer()
#        for elemento in range(len(texto)):
#            texto[elemento] = stem.stem(texto[elemento].lower(), 0, len(texto[elemento])-1)
#        return texto
        return listStemmer.stemmer(texto)
        
            
        
