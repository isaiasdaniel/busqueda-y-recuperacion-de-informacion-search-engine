'''
Created on 30 Apr 2012
@author: isaias
'''
from ProcesadorTexto import Archivo
from ModeloRI import EspacioVectorial
from DataBase import dbCarpetas
from DataBase import dbArchivos
import time

if __name__ == '__main__':
    p = ['100','200','300','400','500','700','900','1000','1500','2000','3000','5000','10000','20000','40000','mas_40000']
    dbc = dbCarpetas.dbCarpetas()
    dba = dbArchivos.dbArchivos()
    dbc.Guardar(('/home/isaias/Documents/TT_segundo_intento/testDoc/',100.0,100.0,100.0))
    for np in p:
        try:
            id = dbc.GuardarArchivo(('/home/isaias/Documents/TT_segundo_intento/testDoc/',np+'Palabras',10.0,10.0,10.0))
            texto = list()
            a = time.time()
            arc = Archivo.Archivo('/home/isaias/Documents/TT_segundo_intento/testDoc/'+np+'Palabras')
            contenido = arc.getListTerminos()
            b = time.time()
            texto = arc.getListTerminosStopWord(contenido)
            c = time.time()
            print texto
            arc.Stemmer(texto)
            print texto
            d = time.time()
            ev = EspacioVectorial.EspacioVectorial()
            e = time.time()        
            cont2 = ev.frecuenciaDocumento(texto)
            f =time.time()
#            print "\nDocumento",np,'Palabras\nNumero de Palabras:',len(contenido),'Palabra a indexar:',len(cont2)
#            print   "Tiempos\n\tExtraccion y filtro:",(b-a)
#            print   "\tElm StopWords",(c-b)
#            print   "\tStemming",(d-c)
#            print   "\tConteo EV:",(f-e)
#            print   "\tTotal:",(f-a)
            dba.SetTerminos(id, cont2)
        except:
            pass
