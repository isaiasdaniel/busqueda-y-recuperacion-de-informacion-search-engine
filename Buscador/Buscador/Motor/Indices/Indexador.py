'''
Created on 30 May 2012

@author: isaias
'''

from ProcesadorTexto import Archivo
from ModeloRI import EspacioVectorial
from DataBase import dbArchivos
from DataBase import dbIndices
from scipy import mat
from scipy import linalg
from scipy import dot
import matrizGen
import os

class Indexador(object):
    '''Encargo de Analizar los contenidos extraidos y determinar la creacion y extraccion
    de indices 
    
    Caso de usos:    RFI-04
                     RFI-05
                     
    Ocupa:
            Extraer Contenido (Procesador de textos)
            Modelo IR (Conteo de Terminod y Frecuencia Inversa de Documentos)
                
    '''

    def __init__(self):
        '''Constructor
        '''
        self.__prosedorTexto = None
        self.__modeloRI = None
        self.__db = dbArchivos.dbArchivos()
        self.__indices = dbIndices.dbIndices()
        
    def CrearIndice(self,idArchivo,root,path,archivo):
        '''RFI-04 Manda a llamar a procesamiento de texto  Obteniendo un 
        diccionario con key termino value ift enviando a una instancia
        del DBManager Para su Manejo en Base.
        
        Args:
            idArchivo:  (strings)Identificador unico de Archivo
            root:       (strings)Direccion raiz
            path:       (strings)Direccion de Carpeta
            archivo:    (strings)Nombre de Archivo
            
        Return:
            None
            
        '''
        print "-> Crear Indices para ",os.path.join(root,path,archivo)  
        terminos = self.ProcesamientoIndexacion(os.path.join(root,path,archivo)) 
        self.__db.SetTerminos(idArchivo,terminos)
        
    def ActualizarIndice(self,idArchivo,root,path,archivo):
        '''RFI-05 llama a ModificacionesContenidos para detectar los cambios dentro de los 
        indices y procede a enviar datos (dict(valores nuevos), dic(valores a eliminar) a DBManager para su manejo en Base de Datos.
        
        Args:
            idArchivo:  (strings)Identificador unico de Archivo
            root:       (strings)Direccion raiz
            path:       (strings)Direccion de Carpeta
            archivo:    (strings)Nombre de Archivo
            
        Return:
            None
        
        '''
        print "-> Actualizando Indices",os.path.join(root,path,archivo)    
        indices = self.ProcesamientoIndexacion(os.path.join(root,path,archivo))
        n,e = self.ModificacionesContenido(self.__db.GetTerminos(idArchivo),indices.items())
        self.__db.EliminarTerminos(idArchivo,e)
        self.__db.SetTerminos(idArchivo,n)
                   
    def ModificacionesContenido(self,dbIndices,nuevosIndices):
        '''Verifica cuales indices fueron eleiminado y cuales son nuevos.
        
        Arg:
            dbIndices: indices almacenados dentro de la una base de datos Lista de tupla [(llave,valor)...]
            nuevosIndices: indices creados por procesamiento de texto (actulizados) Lista de tupla [(llave,valor)...]
            
        Return:
            (nuevos,elimandos), indices nuevos o a actulizar, y los indices a eliminar
        '''
        nuevos = {}
        eliminados = {}
        for k,v in nuevosIndices:
            if not (k,v) in dbIndices:
                nuevos[k]=v
        for k,v in dbIndices:
            if not (k,v) in nuevosIndices:
                eliminados[k]=v
        return (nuevos,eliminados)      
        
    def ProcesamientoIndexacion(self,path):
        '''Llamadas necesarias para Extraccion y filtrado de contenidos en textos planos
        
        Args:
            path: Direccion del Archivo a extraer contenido
        '''
        try:
            self.__procesadorTextos = Archivo.Archivo(path)
            texto = self.__procesadorTextos.getListTerminos()
            texto = self.__procesadorTextos.getListTerminosStopWord(texto)
            #texto = self.__procesadorTextos.Stemmer(texto)
            self.__procesadorTextos.Stemmer(texto)
            self.__modeloRI = EspacioVectorial.EspacioVectorial()
            indice = self.__modeloRI.frecuenciaDocumento(texto)
            return indice
        except:
            print "Error al generar indices"
            
    def GetIndices(self,numArchivos,Vector):
        '''Regresa una matriz de datos en donde las filas son los archivos y las columnas los terminos de estos archivo
        los elementos de esta matriz son el valor del termino en el documento a esta matriz se le apliaca LTA
        
        Args:
            numArchivo: int Entero numero que indica a partir de que archivo desea los datos si este dato es cero regresa 
            10 archivo primeros si es 10 regresa 10 archivos a partir del archiv 10
            Vector: diccionario con el vector de consulta y filtro
       
        Return:  
            Matriz: diccionario la llaves son archivo valores es una llave con las raize y el tf_idf
        '''
        
        Indices = self.__indices.GetIndices(numArchivos,Vector)
        indices,matriz = matrizGen.mtz(Indices)
        matriz = mat(matriz)
        u,e,v = linalg.svd(matriz)
        k = int(linalg.norm(matriz,'fro'))
        e = linalg.diag(e)
        matriz = dot(u[:,:k],dot(e[:k,:k],v[:k,:]))
               
        vect = matrizGen.SetMatrix(indices,matriz.tolist())

        return matrizGen.GetVectores(vect ,Vector['ConsGeneral'])
        
        