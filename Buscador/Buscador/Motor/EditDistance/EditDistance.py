'''
Created on 29 Apr 2012

@author: isaias
'''

from DataBase import dbPalabras
import Levenshtein

class LevenshteinDist(object):
    '''
    Calculo de la distancia de de una o variaz palabras
    '''

    def __init__(self):
        '''
        Constructor
        '''
        db = dbPalabras.dbPalabras()
        self.__palabras = db.getPalabras()
        
    def distance(self,str1,str2):
        '''
        Calcula la distancia entre palabra1 y palabra2
        '''
        d=dict()
        for i in range(len(str1)+1):
            d[i]=dict()
            d[i][0]=i
        for i in range(len(str2)+1):
            d[0][i] = i
        for i in range(1, len(str1)+1):
            for j in range(1, len(str2)+1):
                d[i][j] = min(d[i][j-1]+1, d[i-1][j]+1, d[i-1][j-1]+(not str1[i-1] == str2[j-1]))
        return d[len(str1)][len(str2)]
    
    def correccion(self,palabras):
        '''
        Calcula la mejor opcion para corregir una palabra mal escrita
        '''
        for i in range(len(palabras)):
            opciones = Levenshtein.dist(self.__palabras,palabras[i])
#        for p in self.__palabras: # palabra regresa una lista de tuplas [(palabra,null)...]
#            opciones.append((self.distance(palabra,p[0]),p[0]))
            if opciones:
                opciones.sort()
                palabras[i] = opciones[0][1]
        return palabras

        
    
        