'''
Created on Nov 11, 2012

@author: isaias
'''
from Parser import Parse
from Indices import Indexador
from ModeloRI import similitud, frecDoc
from ProcesadorTexto import removeSW, listStemmer
from EditDistance import EditDistance
from bs4 import BeautifulSoup
import re
import Config as cfg

class Respuesta(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructorgtd
        '''
        self.parse = Parse.Parse()
        self.Indices = Indexador.Indexador()
        self.distance = EditDistance.LevenshteinDist()
        self.__stopWords = cfg.STW
        self.__Root = cfg.PATH
        
    def SugerenciaCorreccion(self,Vector):
        '''
        '''
        sugerencia = ""
        if Vector['ConsGeneral']:
            sugerencia += " ".join(Vector['ConsGeneral'])
        if 'AND' in Vector and Vector['AND']:
            sugerencia += " +"+" +".join(Vector['AND'])
        if 'AND' in Vector and Vector['OR']:
            sugerencia += " |"+" |".join(Vector['OR'])
        if 'AND' in Vector and Vector['NOT']:
            sugerencia += " -"+" -".join(Vector['NOT'])
        return sugerencia
    
    def Construir(self,Consulta,indice):
        ''' Toma una cadena de caracteres
        
        Arg:
            Consulta String
        
        return: 
            lista: de archivo ordenada segun 
        '''

        respuesta = {}
        vectorConsulta = []
        #Analisis de Consulta y construccion de analisis
        Vector = self.parse.buildConsulta(Consulta.lower())
        NewVector = Vector.copy()
        for k,v in Vector.items():
            NewVector[k] = []
            if v:
                # Correccion de terminos
                NewVector[k] += self.distance.correccion(v[:])
                
                # Filtrwo de StopWord
                Vector[k] = removeSW.remove(self.__stopWords,v)
                # Filtro Stemming
                listStemmer.stemmer(Vector[k])
                
        #Sugerencias
        respuesta["Sugerencia"] = self.SugerenciaCorreccion(NewVector)

        #Obtener Indices
        ind = self.Indices.GetIndices(indice,Vector)
        a = frecDoc.fd(Vector['ConsGeneral'])
        for v in Vector['ConsGeneral']:
            vectorConsulta.append(a[v])

        archivos = similitud.cos(tuple(vectorConsulta),ind)
        
        respuesta["Archivos"] = sorted(archivos.items(),key=lambda x:x[1])

        return respuesta
    
    def ConstruirHTML(self,Consulta,indice):
        '''Construye la seccion de resultados con los archivos correspondientes y entrega una string con formato html
        
        Arg:
            Consutla: String con la consulta a realizar
            
        Return:    
            HTML: String cadena de caracteres con los resultados a la consutla en formato html
        '''
        try:
            resultado = self.Construir(Consulta, indice)
            i=indice
            text=''
            if resultado["Sugerencia"]==Consulta.lower():
                resultado["Sugerencia"] = None
            
            newArchivos = []
               
            for item in resultado['Archivos']:
                i+=1
                doc = open(self.__Root+'/'+item[0]).read()
                
                html = BeautifulSoup(open(self.__Root+'/'+item[0]).read())
                try:
                    newArchivos.append(item+(html.title.text,)+(re.sub(r'\n+', ' ...', '...'+html.body.text[200:700]),))
                except AttributeError:
                    newArchivos.append(item+(html.title.text,)+(re.sub(r'\n+',' ...',re.sub('\n+',' ',re.sub('<[^>]*>','',doc))[0:500]),))
                    
            
            resultado['Archivos'] = newArchivos
            
            return resultado
        except ValueError:
            return '<p class="SinResultado"><h1 id="no"> Hooo nooooo!!!!</h1> <h2 id="no">No hay Resultado para esta consulta</h2> </p>'
            
