'''
Created on 17 May 2012

@author: isaias
'''
import frecDoc

class EspacioVectorial(object):
    '''Calculos necesarios para crear los vectores descriptores de los documentos
    
    Caso de uso: 
                    RFI-09
                    RFI-10
                    RFI-11
    '''


    def __init__(self):
        '''            
        Constructor
        '''
        self.vector = dict()
        
    def frecuenciaDocumento(self,texto):
        '''Toma un arreglo de palabras texto y regresa un diccionario donde cada key
        es una palabra dentro del texto y su valor en numero de apariciones
        
        Args: 
            texto: lista con palabras pertenecientes a un texto (archivo en texto
                 plano o formato manejado por el procesador de texto)
            
        Return:
            Diccionario key termino value numero de apariciones.
        '''
#        for termino in set(texto):
#            self.vector[termino] = 0.0        
#        for key in self.vector.keys():
#            for termino in texto:
#                if key.lower()==termino.lower():
#                    self.vector[key] +=1.0        
#        self.normalizado(self.vector[max(self.vector,key=self.vector.get)])
#        return self.vector
        self.vector = frecDoc.fd(texto)        
        return self.vector#frecDoc.fdNormalizador(self.vector,self.vector[max(self.vector,key=self.vector.get)]);

    
    def normalizado(self, maximo):
        '''Normaliza el diccionario de frecuencia de terminos
        
        Args: 
            maximo: el valor del termino que aparecion mas veces dentro del texto
        
        return:
            self.vector (nomralizado con valores del 0 al 1
        '''
        for key in self.vector.keys():
            self.vector[key] = self.vector[key]/maximo
            
        
        
        
    