#####################################################################
#                                                                   #
# Descripcion: Detecta operadores Booleanos y                       #
#            Crea vectores de consultas                             #
#                                                                   #
#####################################################################
import re

class Parse():
    '''Detecta Operadores Booleanos y Crea vectores de Consultas En una consulta simple es decir sin operadores booleanos
    todos los espacios en blanco son tratados como OR
    
    Operadores 
        + = AND
        | = OR
        - = NOT
    
    Estructura ideal de una consulta
    
    Consulta general [Operador]["Consulta Frase"|palabra]+
    Casos de Uso RFR02 y RFR03
    
    Atributos:
        self.__frase: string que contiene la frase a convertir en consultapa
        self.__operadoresORQL: expresion regular para extraer consultas OR del tipo |"Frase (varias Palabras)"
        self.__operadoresOR: expresion regular para extraer consultas OR simpres |palabra
        self.__operadoresANDQL: expresion regular para extraer consultas AND del tipo +"Frase (varias Palabras)"
        self.__operadoresAND: expresion regular para extraer consultas AND simples +palabra
        self.__operadoresNOTSQL: expresion regular para extraer consultas NOT del tipo -"Frase (varias Palabras)"
        self.__operadoresNOT: expresion regular para extraer consultas NOT simple -palabra
        self.__Consulta: expresion regular para extraer expresion general
    '''
    
    def __init__(self):
        '''Constructor'''
        self.__frase = None
        self.__operadores = {}
        self.__operadores['ORQ'] = re.compile('\|\"(.+?)\"')
        self.__operadores['OR'] = re.compile('\|([A-Za-z0-9]+)')
        self.__operadores['ANDQ'] = re.compile('\+\"(.+?)\"')
        self.__operadores['AND'] = re.compile('\+([A-Za-z0-9]+)')
        self.__operadores['NOTQ'] = re.compile('\-\"(.+?)\"')
        self.__operadores['NOT'] = re.compile('\-([A-Za-z0-9]+)')
        self.__consulta = re.compile('^(.+?)\s[\||\+|\-]')
        
    def buildConsulta(self,consulta):
        ''' Convierte las consultas en Vectores de Consultas
        
        Arg:
            consulta: String
            
        Return:
            vectConsutlas: diccionario'''
              
        if (re.match(r'.*\s[\+|\-|\|].+',consulta)):
            return self.__ConstruirSintactico(consulta)
        else:
            return {'ConsGeneral':consulta.split(' ')}

    def getConsutas(self,consulta):
        ''''''
        return self.buildConsulta(consulta)
    
    def __ConstruirSintactico(self,consulta):
        ''' Convierte un string en una estructura dependiendo la sintaxis de la cadena para la creacion de vetores de consultas
                
        Args:
            consulta: String
            
        Return:
            arbSinta: estructura de Arbol para creacion de consultas.
            
        '''
        subConsultasOR = self.__operadores['ORQ'].findall(consulta)
        subConsultasAND = self.__operadores['ANDQ'].findall(consulta)
        subConsultasNOT = self.__operadores['NOTQ'].findall(consulta)
        vectorConsultas = {'ConsGeneral':[],'OR':[],'AND':[],'NOT':[]}
        for subCons in subConsultasOR:
            if not (re.match(r'.*\s[\+|\-|\|].+',subCons)):
                vectorConsultas['OR'] += subCons.split(' ')
        for subCons in subConsultasAND:
            if not (re.match(r'.*\s[\+|\-|\|].+',subCons)):
                vectorConsultas['AND'] += subCons.split(' ')
        for subCons in subConsultasNOT:
            if not (re.match(r'.*\s[\+|\-|\|].+',subCons)):
                vectorConsultas['NOT'] += subCons.split(' ')
        vectorConsultas['AND'] += self.__operadores['AND'].findall(consulta)
        vectorConsultas['NOT'] += self.__operadores['NOT'].findall(consulta)
        vectorConsultas['OR'] += self.__operadores['OR'].findall(consulta)
        vectorConsultas['ConsGeneral'] += self.__consulta.findall(consulta)[0].split(' ')
        return vectorConsultas
        
        

    
    
    