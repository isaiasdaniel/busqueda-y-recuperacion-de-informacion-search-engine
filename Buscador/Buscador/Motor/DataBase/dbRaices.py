'''
Created on 2 Jun 2012

@author: isaias
'''
import dbManager

class dbRaices(dbManager.dbManager):
    '''
    classdocs
    '''

    def __init__(self):
        '''Constructor
        '''
        try:
            super(dbRaices,self).__init__()
        except:
            pass
    
    def ExisteRegistroRaiz(self,registro):
        ''' Recibe una raiz a verificar si existe dentro de la base de datos
        '''
        try:
            return super(dbRaices,self).ExisteRegistro("SELECT * FROM Raices WHERE raiz='%s';"%(registro))
        except:
            pass
    
    def GetIDRaiz(self,registro):
        '''Recibe una registro palabra raiz consulta
        '''
        try:
            return super(dbRaices,self).GetID("SELECT raiz_id FROM Raices WHERE raiz='%s';"%(registro))
        except:
            pass  
    
    def GuardarRaiz(self,registro):
        '''Recibe una raiz a guardar
        '''
        try:
            raiz = self.ExisteRegistroRaiz(registro)
            if not raiz:
                super(dbRaices,self).Guardar("INSERT INTO Raices(raiz,idf) VALUES ('%s',%f);"%(registro,1.0))
            else:            
                super(dbRaices,self).Guardar("UPDATE Raices SET idf=(SELECT count(*) FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id=t.archivo_id and t.raiz_id=r.raiz_id and raiz='%s')+1 WHERE raiz='%s';"%(registro,registro))
            return self.GetIDRaiz(registro)
        except:
            pass
    
    def EliminarRaiz(self,registro):
        '''
        ''' 
        try:
            raiz = self.ExisteRegistroRaiz(registro)
            self.Modificacion("UPDATE Raices SET idf=(SELECT count(*) FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id=t.archivo_id and t.raiz_id=r.raiz_id and r.raiz='%s')-1 WHERE raiz='%s';"%(registro,registro))
        except:
            pass
        
    def get_N(self, raiz=None):
        ''' Obtiene todos los archivos que contiene una raiz
    
        Args:
            raiz: String Raiz que debera contener el archivo si es none obtiene todos los archivos
            
        return:
            int: numero de archivos que contienen la raiz
        '''
        try:
            if raiz:
                consulta = "SELECT count(*) FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz = '"+raiz+"';"
            else:
                consulta = "SELECT count(*) FROM Archivos;"
            return super(dbRaices,self).Consulta(consulta)[0][0]
        except:
            pass
    
    def get_N00(self,termino1,termino2):
        ''' Obtiene todos los archivos que no contiene los terminos
    
        Args:
            raiz: String Raiz que debera contener el archivo si es none obtiene todos los archivos
            
        return:
            int: numero de archivos que contienen la raiz
        '''
        try:
            consulta = "SELECT count(*)-(SELECT count(DISTINCT a.archivo_id) FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz in ('"+termino1+"','"+termino2+"')) FROM Archivos;"
            return super(dbRaices,self).Consulta(consulta)[0][0]
        except:
            pass
        
    def get_N11(self,termino1,termino2):
        ''' Obtiene todos los archivos que contiene los terminos
    
        Args:
            raiz: String Raiz que debera contener el archivo si es none obtiene todos los archivos
            
        return:
            int: numero de archivos que contienen la raiz
        '''
        try:
            consulta = "SELECT count(*) FROM (SELECT a.archivo_id FROM Archivos a, Raices r, Terminos t WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz='"+termino1+"') t1 INNER JOIN (SELECT a.archivo_id FROM Archivos a, Raices r, Terminos t WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz='"+termino2+"') t2 ON (t1.archivo_id = t2.archivo_id);"         
            return super(dbRaices,self).Consulta(consulta)[0][0]
        except:
            pass

    def get_N01N10(self,termino1,termino2):
        ''' Obtiene todos los archivos que contiene los terminos
    
        Args:
            raiz: String Raiz que debera contener el archivo si es none obtiene todos los archivos
            
        return:
            int: numero de archivos que contienen la raiz
        '''
        try:
            consulta = "SELECT count(*)-(SELECT count(t1.archivo_id) FROM (SELECT a.archivo_id FROM Archivos a, Raices r, Terminos t WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz='"+termino1+"') t1 INNER JOIN (SELECT a.archivo_id FROM Archivos a, Raices r, Terminos t WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz='"+termino2+"') t2 ON (t1.archivo_id = t2.archivo_id)) FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz = '"+termino2+"';"         
            N10 = float(super(dbRaices,self).Consulta(consulta)[0][0])
            consulta = "SELECT count(*)-(SELECT count(t1.archivo_id) FROM (SELECT a.archivo_id FROM Archivos a, Raices r, Terminos t WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz='"+termino1+"') t1 INNER JOIN (SELECT a.archivo_id FROM Archivos a, Raices r, Terminos t WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz='"+termino2+"') t2 ON (t1.archivo_id = t2.archivo_id)) FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id = t.archivo_id and t.raiz_id = r.raiz_id and r.raiz = '"+termino1+"';"         
            N01 = float(super(dbRaices,self).Consulta(consulta)[0][0])
            return (N10,N01)
        except:
            pass
    
    def bestRankword(self):
        ''' Obtienen la mejores palabras conlos mejores ranfo de ft_idf
        
        Args: None
        
        Return:
            List: info  mutua
        '''
        try:
            consulta = 'SELECT DISTINCT r.raiz FROM Raices r, Terminos t WHERE r.raiz_id=t.raiz_id and (t.ft*log((SELECT count(*) FROM Archivos)/r.idf)>0.6);'
            return super(dbRaices,self).Consulta(consulta)
        except:
            pass
        
    def ExisteInfoMutua(self,registro):
        ''' Recibe una registro a verificar si existe dentro de la base de datos
        '''
        try:
            return super(dbRaices,self).ExisteRegistro("SELECT * FROM informutua WHERE raiz_id='%s' and raizinformutua_id='%s';"%(registro))
        except:
            pass
    
    def GetIDinfoMutua(self,registro):
        ''' Recibe una registro palabra raiz consulta
        '''
        try:
            return super(dbRaices,self).GetID("SELECT raiz_id FROM informutua WHERE raiz_id='%s' and raizinformutua_id ='%s';;"%(registro))
        except:
            pass
    
    def GuardarInfoMutua(self,registro):
        ''' Guarda la informacion mutua
        
        Args:
            registro: tupla (raiz,raiz,infoMutua)
            
        Return: 
            id: registro
        '''
        try:
            raiz1id = self.GetIDRaiz(registro[0])
            raiz2id = self.GetIDRaiz(registro[1])
            raiz = self.ExisteInfoMutua((raiz1id,raiz2id))
            if not raiz:
                super(dbRaices,self).Guardar("INSERT INTO informutua (raiz_id,raizinformutua_id,relacion ) VALUES ('%s','%s',%f);"%(raiz1id,raiz2id,registro[2]))
            else:            
                super(dbRaices,self).Guardar("UPDATE informutua SET relacion=%f WHERE raiz_id='%s' and raizinformutua_id ='%s';"%(registro[2],raiz1id,raiz2id))
            return self.GetIDinfoMutua((raiz1id,raiz2id))
        except:
            pass
        
         
         
         
    
        
        
        