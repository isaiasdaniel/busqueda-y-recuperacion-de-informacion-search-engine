'''
Created on 24 May 2012

@author: isaias
'''
import dbRaices

class dbTerminos(dbRaices.dbRaices):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        super(dbTerminos,self).__init__() 
    
    def ExisteRegistro(self,registro):
        '''Comprueba si el registro existe
        
        Arg:
            registro: tupla (idArchivo,termino)
            
        Return
            False: si la no existe el termino com raiz o no existe la relacion entre la raiz y el archivo
            tupla: tupla con los datos de la relacion entre el archivo y la relacion
        '''
        try:
            idArchivo,termino = registro
            datosRaiz = self.ExisteRegistroRaiz(termino)
            if datosRaiz:
                return super(dbTerminos,self).ExisteRegistro("SELECT * FROM Terminos WHERE archivo_id=%i AND raiz_id=%i;"%(idArchivo,datosRaiz[0]))
            return False
        except:
            pass
    
    def GetID(self,registro):
        ''' Obtiene el id de relacion entre la raiz y el archivo
        
        Arg:
            registro: Tupla (idArchivo,idRaiz)
            
        return
            int: entero si existe la relacion
            False: si no existe
        '''
        try:
            return super(dbTerminos,self).GetID("SELECT termino_id FROM Terminos WHERE archivo_id=%i AND raiz_id=%i;"%(registro))
        except:
            pass  
                
    def Guardar(self,registro,**args):
        '''Guarda o actualiza la raiz y guardar o actualiza la relacion entre la raiz y el archivo
        
        Arg:
            registro: Tupla (idArchivo,termino)
            
        Return:
            id: Entero de la relacion entre archivo y termino
        '''
        try:
            if not args:
                self.__GuardarRegistro(registro)
            elif ('lista' in args) and (args['lista']):
                self.__GuardarLista(registro,args['lista'])
            else:
                print "Archivo sin cambios (Terminos nuevos)"
        except:
            pass
    
    def __GuardarLista(self,registro,lista):
        ''' Guarda varios registros de terminos en la base de datos
        
        Arg:
            registro: id de Archivo
            lista: diccionarios dondel la llave es el termino y el valor el ft
            
        Return:
            None
        '''
        
        sql = ''
        self.cur.execute('BEGIN;')
        for key,value in lista.items():
            idTermino = self.GuardarRaiz(key)
            datosTerminos = self.ExisteRegistro((registro,key))
            if not datosTerminos:
                sql = sql + 'INSERT INTO Terminos(raiz_id,archivo_id,ft) VALUES(%i,%i,%f);'%(idTermino,registro,value)
            else:
                sql = sql + "UPDATE Terminos SET ft=%f WHERE raiz_id=%i AND archivo_id=%i;"%(value,idTermino,registro)
        super(dbTerminos,self).Guardar(sql)
        self.cur.execute('COMMIT;')
                
    def __GuardarRegistro(self,registro):
        '''Guarda o actualiza el registro
        
        Arg:
            registro: Tupla (idArchivo,termino)
            
        Return:
            id: Entero de la relacion entre archivo y termino
        '''
        try:
            idArchivo,termino,ft = registro
            idTermino = self.GuardarRaiz(termino)
            datosTerminos = self.ExisteRegistro((idArchivo,termino))
            if not datosTerminos:
                super(dbTerminos,self).Guardar('INSERT INTO Terminos(raiz_id,archivo_id,ft) VALUES(%i,%i,%f);'%(idTermino,idArchivo,ft))
            else:
                super(dbTerminos,self).Guardar("UPDATE Terminos SET ft=%f WHERE raiz_id=%i AND archivo_id=%i;"%(ft,idTermino,idArchivo))
            return self.GetID((idArchivo,idTermino))
        except Exception, e:
            print "Aqui hay un errorsaso",str(e)
    
    def Eliminar(self,registro,**args):
        '''Elimina o la o el termino relacionado a un archivo
        
        Arg: 
            registro:(idArchivo,termino)
        
        Return:
            None
        '''
        try:
            if not args:
                self.__EliminarRegistro(registro)
            elif ('lista' in args) and (args['lista']):
                self.__EliminarLista(registro,args['lista'])
            else:
                print "Archivo sin cambios (Terminos a eliminar)"
        except:
            pass
        
    def __EliminarLista(self,registro,lista):
        '''Elimina varios registros de terminos en la base de datos
        
        Arg:
            registro: idArchivo
            lista: lista (Terminos)
            
        Return:
            None
        '''
        sql = '' 
        for termino in lista:
            sql = sql + "DELETE FROM Terminos WHERE raiz_id=%i AND archivo_id=%i;"%(self.GetIDRaiz(termino),registro)
        super(dbTerminos,self).Eliminar(sql)   
                
    def __EliminarRegistro(self,registro):
        '''Elimina la relacion entre la raiz y el archivo
        
        Arg:
            registro: Tupla (idArchivo,Termino)
            
        Return:
            None
        '''
        try:
            idArchivo,termino = registro
            super(dbTerminos,self).Eliminar("DELETE FROM Terminos WHERE raiz_id=%i AND archivo_id=%i;"%(self.GetIDRaiz(termino),idArchivo))
            self.EliminarRaiz(termino)
        except:
            pass

    def GetTerminosft(self,registro):
        '''Obtiene todos los terminos relacionados a un archivo
        
        Arg:
            registro: idArchivo
            
        Return:
            None
        '''
        try:
            return self.Consulta("SELECT Raices.raiz,Terminos.ft FROM Terminos,Raices WHERE Terminos.archivo_id = %i AND Terminos.raiz_id = Raices.raiz_id;"%registro)
        except:
            pass
        
        