'''
Created on Nov 10, 2012

@author: isaias
'''
import dbManager    
class dbIndices(dbManager.dbManager):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        try:
            super(dbIndices,self).__init__()
        except:
            pass 
            
    def GetConsuta(self,indice, **kargs):
        '''Obtiene la consutla para extraer los indices de la base de datos
        
        Arg:
            **kargs: diccionario con los valores AND NOT OR Y GENERAL
            
        Rerturn:
            String: Consulta a ejecutar
        '''
        if 'NOT' in kargs and kargs['NOT']:
            ConsultaNOT = self.GetConsultaNOT(kargs['NOT'])
        else:
            ConsultaNOT = ''
        if 'AND' in kargs and kargs['AND']:
            consultaAND,CondicionAND = self.GetConsultaAND(kargs['AND'])
        else:
            consultaAND,CondicionAND = '',''
        if 'OR' in kargs and kargs['OR']:
            kargs['ConsGeneral']+=kargs['OR']
        if 'ConsGeneral' in kargs:
            return self.GetConsultaGeneral(self.GetConsultaOR(kargs['ConsGeneral']),ConsultaNOT,consultaAND,CondicionAND,indice)

    def GetConsultaGeneral(self,ConsultaGeneral,ConsultaNot=None,ConsultaAND=None,CondicionAND=None,indice=10):
        '''Obtiene la consulta general
        
        Args:
            ConsultaGeneral: lista de palabras de una consulta general junto con las consulta pon operador OR
            ConsultaNOT: Subquery que es usada con la consulta general
            ConsultaAND: Subquery que es usada con la consulta general
            CondicionAND: Condicion usada solo cuando en la cuando en la consulta general se unas uns subquery AND
        '''
        return "SELECT A.nombre, r.raiz, t.ft*log((SELECT count(*) FROM Archivos)/r.idf) FROM (SELECT (C.carpeta||'/'||A.nombre) as nombre, A.archivo_id,  count(*) as num, sqrt(sum(pow(t.ft*log((SELECT count(*) FROM Archivos)/r.idf),2.0))) as sum FROM Carpetas C, Archivos A, Terminos t, Raices r %s WHERE %s %s C.carpeta_id = A.Carpeta_id AND a.archivo_id = t.archivo_id AND t.raiz_id = r.raiz_id AND r.raiz IN %s GROUP BY A.archivo_id,C.carpeta ORDER BY  num desc, sum desc OFFSET %d LIMIT  10 ) A, Terminos t, Raices r WHERE A.archivo_id = t.archivo_id AND t.raiz_id = r.raiz_id AND r.idf<>0"%(ConsultaAND,ConsultaNot,CondicionAND,ConsultaGeneral,indice)
                    
    def GetConsultaOR(self,lista):
        '''Crear string para ser usados en la consulta general
        
        Arg: 
            lista para convertir lista en consultas'''
        if len(lista)==1:
            return "('"+lista[0]+"')"
        elif len(lista)>1:
            return str(tuple(lista))

    def GetConsultaAND(self,lista):
        '''Crea la consulta para AND
        
        Args:
            lista: lista donde cada elemento es una palabra que debe tener el documento a buscar
            
        Return:
            tupla: tupla con dos elemento cada uno parte de la consulta general
        '''
        
        if len(lista)==1:
            sq = "('"+lista[0]+"')",1
        elif len(lista)>1:
            sq = tuple(lista),len(lista)
        return (",(SELECT archivos.archivo_id FROM archivos, terminos, raices WHERE archivos.archivo_id = terminos.archivo_id AND raices.idf<>0 AND terminos.raiz_id = raices.raiz_id AND raices.raiz in %s GROUP BY archivos.archivo_id HAVING count(*) = %d) SQ_AND"%sq,"a.archivo_id = SQ_AND.archivo_id AND")
        
    def GetConsultaNOT(self,lista):
        '''Crea la consulta para NOT
        
        Args:
            lista: lista donde cada elemento es una palabra que debe tener el documento a buscar
            
        Return:
            tupla: tupla con dos elemento cada uno parte de la consulta general
        '''
        if len(lista)==1:
            sq = "('"+lista[0]+"')"
        elif len(lista)>1:
            sq = str(tuple(lista))
        return "A.archivo_id not in ((SELECT a.archivo_id FROM Archivos a, Terminos t, Raices r WHERE a.archivo_id=t.archivo_id AND t.raiz_id=r.raiz_id AND r.raiz IN %s GROUP BY a.archivo_id)) AND"%sq

    def GetIndices(self,indice,Vector):
        ''' Obtiene los indices archivos y terminos correcpondientes a los vectores de consultas
        
        Arg:
            indice: a partir de que dato inicia la recuperacion de archivos
            Vector: diccionarion con los vectores de consultas
    
        Return:
            Lista: con tuplas que cumplen con los vectores de consultas
        '''
        return self.Consulta(self.GetConsuta(indice, **Vector))