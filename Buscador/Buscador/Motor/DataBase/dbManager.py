
'''
Created on 29 Apr 2012

@author: Isaias Daniel Garcia Rodriguez
'''

import psycopg2

class dbManager(object):
    ''' Administracion y manejo de la base datos
    '''
    
    def __init__(self):
        ''' Constructor
        '''
        try:
            self.conn = psycopg2.connect("dbname=indices user=isaias") #Puede ser isaias o daniel depende de la configuracion
            self.cur = self.conn.cursor()
        except Exception,e:
            print "Error con el modulo dbManager "+str(e)
            
    def ExisteRegistro(self,registro):
        ''' Comprueba si registro existe dentro de la base de datos
        
        Arg:
            registro: Consulta SQL
        
        Return:
            False:    si no hay datos en la base de datos
            Tupla:    Tupla con los datos del registro
            
        Note: "datos" es una lista de tuplas y solo se toma el primer elemento    
        '''
        try:
            self.cur.execute(registro)
            datos = self.cur.fetchall()
            if datos:
                return datos[0]
            else:
                return False
        except Exception,e:
            print "Error!! no se puede determinar si el registros Existe!!\n\st"+str(e)
            return None
        
    def GetID(self,registro):
        ''' Consulta el id de un registro
        
        Arg:
            registro: Consulta SQL
        
        Return:
            False:    si no hay datos en la base de datos
            int:      Entero id
            
        Note:     
        '''        
        try:
            self.cur.execute(registro)
            datos = self.cur.fetchall()
            if datos:
                return datos[0][0]
            else:
                return False
        except Exception,e:
            print "Error!! no se puede obtener el id "+str(e)
            return None
        
    def Guardar(self,registro,**args):
        ''' Ejecuta la instruccion SQL para guardar o actualizar un registro
        
        Arg:
            registro: Instruccion SQL "INSERT INTO ..."
            
        Return:
            None  
        
        Note:
        '''
        try:
            self.cur.execute(registro)
            self.conn.commit()
        except Exception,e:
            print "Error!! no se puede Guardar el registro "+str(e)
        
    def Eliminar(self,registro):
        '''Ejecuta la instruccion SQL para Eliminar un registro !!! XD
        
        Arg:
            registro: Instruccion SQL "INSERT INTO ..."
            
        Return:
            None  
        
        Note:
        '''
        try:
            self.cur.execute(registro)
            self.conn.commit()
        except Exception,e:
            print "Error!! no se puede Eliminar el registro "+str(e)
        
    def Consulta(self,registro):
        ''' Ejecuta una consulta SQL
       
        Arg:
            registro: Consulta SQL "SELECT ... FROM ..."
            
        Return:
            list(tuple): Regresa una lista con tuplas segun se especifico en la consulta 
        
        Note:
        '''
        try:
            self.cur.execute(registro)
            return self.cur.fetchall()
        except Exception,e:
            print "Error!! realizando la consulta a la base de datos "+str(e)
    
    def Modificacion(self,registro):
        ''' Ejecuta una consulta SQL
       
        Arg:
            registro: Instruccion SQL "INSERT, UPDATE, DELETE"
            
        Return:
            None            
        
        Note:
        '''
        try:
            self.cur.execute(registro)
            self.conn.commit()
        except Exception,e:
            print "Error!! realizando la consulta a la base de datos "+str(e)

        
            