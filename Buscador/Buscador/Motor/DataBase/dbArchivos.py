'''
Created on 30 May 2012

@author: isaias
'''
import dbManager
import dbTerminos

class dbArchivos(dbManager.dbManager):
    '''
    classdocs
    '''

    def __init__(self):
        '''Constructor
        '''
        try: 
            super(dbArchivos,self).__init__()
            self.__terminos = dbTerminos.dbTerminos()
        except:
            pass
    
    def ExisteRegistro(self,registro):
        ''' Comprueba si existe un archivo asociado a una carpeta
        
        Arg:
            registro: tupla(idCarpeta,archivo)
            
        Return:
            tupla: si los datos existen
            False: si los datos no existen
            
        Note:
        '''
        try:
            idCarpeta,archivo = registro
            return super(dbArchivos,self).ExisteRegistro("SELECT * FROM Archivos WHERE carpeta_id=%i AND nombre='%s';"%(idCarpeta,archivo))
        except:
            pass
    
    def GetID(self,registro):
        ''' Obtiene el id de un archivo asociado a una carpeta
        
        Arg:
            registro: tupla (idCarpeta,archivo)
            
        Return:
            id: si el archivo existe
            False : si el archivo no existe
        '''
        try:
            idCarpeta,archivo = registro
            return super(dbArchivos,self).GetID("SELECT archivo_id FROM Archivos WHERE carpeta_id=%i AND nombre='%s';"%(idCarpeta,archivo))
        except:
            pass  
    
    def Guardar(self,registro,**args):
        ''' Guarda o Actualiza el registro dentro con su asociacion de carpetas
        
        Arg:
            registro: tupla (idCarpeta,archivo,tamano,atime,mtime)
            
        Return:
            id: Entero id pertenenciente al archiv guardado o actualizado
        '''
        try:
            idCarpeta,archivo,tamano,atime,mtime = registro
            if not self.ExisteRegistro((idCarpeta,archivo)):
                super(dbArchivos,self).Guardar("INSERT INTO Archivos(carpeta_id,nombre,tamano,atime,mtime) VALUES (%i,'%s',%i,%f,%f);"%(idCarpeta,archivo,tamano,atime,mtime))
            else:
                super(dbArchivos,self).Guardar("UPDATE Archivos SET tamano=%i, atime=%f, mtime=%f WHERE nombre='%s' AND carpeta_id=%i;"%(tamano,atime,mtime,archivo,idCarpeta))
            return self.GetID((idCarpeta,archivo))
        except:
            pass
    
    def Eliminar(self,registro):
        ''' Elimina un registro de la db
        
        Arg:
            registro: tupla (idCarpeta,archivo)
            
        Return:
            None
        '''
        try:
            idCarpeta,archivo = registro
            Terminos = self.GetTerminos(self.GetID(registro))
            for termino,x in Terminos:
                self.__terminos.EliminarRaiz(termino)
            super(dbArchivos,self).Eliminar("DELETE FROM Archivos WHERE nombre='%s' AND carpeta_id=%i"%(archivo,idCarpeta))
        except:
            pass
        
    def GetArchivos(self,registro):
        ''' Obtiene todos los archivos relacionasdos a una carpeta
        
        Arg:
            registro: id entero identificador de carpeta
            
        Return:
            List(tuplas): Regresa una lista de tuplas con los datos de la consulta
        '''
        try:
            return self.Consulta("SELECT archivo_id,nombre FROM Archivos WHERE carpeta_id=%i;"%(registro))
        except:
            pass
        
    def GetTerminos(self,registro):
        ''' Obtiene todos los terminos relacionados a un archivo
        
        Arg:
            registro: idArchivo
            
        Return:
            List(tuplas): Regreas una lista de tuplas con los terminos y la frecuencia de terminos ft
        '''  
        try:
            return self.__terminos.GetTerminosft(registro)
        except:
            pass  
    
    def SetTerminos(self,registro,terminos):
        ''' Guarda un en base todos los terminos relacionados a un archivo
        
        Arg:
            registro: idArchivo
            termino: diccionarios con el termino y su ft
            
        Return:
            None            
        '''
        try:
            self.__terminos.Guardar(registro,lista=terminos)
        except:
            pass
        
    def EliminarTerminos(self,registro,terminos):
        ''' Elimina un en base todos los terminos relacionados a un archivo
        
        Arg:
            registro: idArchivo
            termino: diccionarios con el termino y su ft
            
        Return:
            None            
        '''
        try:
            self.__terminos.Eliminar(registro,lista=terminos)
        except:
            pass

        