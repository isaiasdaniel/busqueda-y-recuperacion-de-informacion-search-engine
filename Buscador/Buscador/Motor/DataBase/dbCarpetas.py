'''
Created on 24 May 2012

@author: isaias
'''
import dbManager
import dbArchivos
class dbCarpetas(dbManager.dbManager):
    '''
    classdocs
    '''
    
    def __init__(self):
        ''' Constructor
        '''
        try:
            super(dbCarpetas,self).__init__()
            self.__archivos = dbArchivos.dbArchivos()
        except:
            pass
        
    def ExisteRegistro(self,registro):
        '''Genera la consulta para comprobar la existencia de la carpeta
        
        Arg:
            registro: direccion de la carpetas a buscar "/carpeta/sub_carpeta/...."
            
        Return:
            Tupla:    Tupla con los datos si existe
            False:    Si la carpeta no existe
        '''
        try:
            return super(dbCarpetas,self).ExisteRegistro("SELECT * FROM Carpetas WHERE carpeta='"+registro+"';")
        except:
            pass
    
    def GetID(self,registro):
        '''Genera la consulta para obtenet el id de la carpeta
        
        Arg:
            registro: direccion de la carpetas a buscar "/carpeta/sub_carpeta/...."
            
        Return:
            id: Entero id de la carpeta si existe
            False: si no existe la carpeta
        '''
        try:
            return super(dbCarpetas,self).GetID("SELECT carpeta_id FROM Carpetas WHERE carpeta='"+registro+"';")
        except:
            pass        
            
    def Guardar(self,registro,**args):
        '''Genera la instruccion para guardar o actualizar una carpeta
        
        Arg:
            registro: Tupla (carpeta,tamano,atime,mtime)
            
        Return
            id: Id con el que fue gurdado o actualizado
        '''
        try:
            carpeta,tamano,atime,mtime = registro
            if not self.ExisteRegistro(carpeta):
                super(dbCarpetas,self).Guardar("INSERT INTO Carpetas(carpeta,tamano,atime,mtime) VALUES ('%s',%i,%f,%f);"%(carpeta,tamano,atime,mtime))
            else:
                super(dbCarpetas,self).Guardar("UPDATE Carpetas SET tamano=%i, atime=%f, mtime=%f WHERE carpeta='%s';"%(tamano,atime,mtime,carpeta))
            return self.GetID(carpeta)
        except:
            pass
    
    def Eliminar(self,registro):
        '''General la instruccion para eliminar una carpeta
        
        Arg:
            registro:  direccion de la carpetas a buscar "/carpeta/sub_carpeta/...."
        
        Return:
            None            
        '''
        try:
            super(dbCarpetas,self).Eliminar("DELETE FROM Carpetas WHERE carpeta='%s'"%registro)
        except:
            pass
    
    def ExiteArchivo(self,registro):
        ''' Verifica si dentro de la carpeta  exite el archivo indicado como parametro
        
        Arg:
            registro: Tupla (carpeta,archivo)
            
        Return:
            Tupla: datos del Archivo si existen
            False: si el archivo no existe
        '''
        try:
            carpeta,archivo = registro
            return self.__archivos.ExisteRegistro((self.GetID(carpeta),archivo))
        except:
            pass

    def GuardarArchivo(self,registro):
        '''Guarda el archivo con la referencia en la carpeta indicado en el parametro
        
        Arg:
            registro: tupla(carpeta,archivo,tamano,atime,mtime)
            
        Return:
            id:  Entero con el que guardo o actualizo el Archivo
        '''
        try:
            carpeta,archivo,tamano,atime,mtime = registro
            return self.__archivos.Guardar((self.GetID(carpeta),archivo,tamano,atime,mtime))
        except:
            pass

    def EliminaArchivo(self,registro):
        '''Elimina de una carpeta de su registro
        
        Arg:
            registr: Tupla(carpeta,archivo)
            
        Return:
            None
        '''
        try:
            carpeta,archivo = registro
            self.__archivos.Eliminar((self.GetID(carpeta),archivo))
        except:
            pass   
        
    def GetArchivos(self,registro):
        '''Obtiene tods los archivos relacionados a una carpeta
        
        Arg:
            registro: Carpetas '/Hola/Mundo/Carpeta'
            
        Return:
            list(tuplas): Listas de tuplas (idArchivo,nombre)
            False: si la carpeta no existe
        '''
        try:
            carpetas = self.__archivos.GetArchivos(self.GetID(registro))
            if carpetas:
                return carpetas
            else:
                return False
        except:
            pass
    
    def GetCarpetasArchivos_ALL(self):
        '''Obtiene un diccionario con la carpeta y los documentos relacionados a las carpetas
        
        Arg:
            None
        
        Return:
            dict(key=carpeta,value=list(Archivos...))
        '''
        try:
            ff = self.Consulta('SELECT Carpetas.carpeta,Archivos.nombre FROM Carpetas,Archivos WHERE Carpetas.carpeta_id=Archivos.carpeta_id;')
            return ff
        except:
            pass
    
    def GETCarpetas_ALL(self):
        '''Obtiene todas las carpetas almacendas en la base de datos'''
        try:
            cc = self.Consulta('SELECT carpeta FROM Carpetas;')
            return cc
        except:
            pass
            