#
# Calculo de la informacion mutua entre dos palabras
#

from DataBase import dbRaices
from math import log

class infoMutua:
    ''' Calculos de informacion mutua con los terminos obtenidos para generar sugerencias de
    Busquesda
    
    Attr:
        N : total de documentos
        NOO : numero de documentos que no contiene los terminos
        N11 : numero de documentos que contienen los dos terminos
        N10 : numero de documentos que contienen solo el primer termino
        N01 : numero de documentos que contienen solo el segundo termino
        N1_ : numero de documentos donde aparece el prime termino
        N_1 : numero de documentos donde aparece el segundo termino
    
    '''
    
    def __init__(self):
        ''' Constructor '''
        self.__raices = dbRaices.dbRaices()

        
    def __getDatos(self,registro):
        ''' Obtiene variables para calcular informacion Mutua
        
        Args: 
            registro: (tupla) (termino1,termino2) 
         
        return:
            None
        '''
        self.__N = float(self.__raices.get_N())
        self.__N1_ = float(self.__raices.get_N(registro[0]))
        self.__N_1 = float(self.__raices.get_N(registro[1]))
        self.__N0_ = float(self.__N - self.__N1_)
        self.__N_0 = float(self.__N - self.__N_1)
        self.__N00 = float(self.__raices.get_N00(*registro))
        self.__N11 = float(self.__raices.get_N11(*registro))
        self.__N10,self.__N01 =  self.__raices.get_N01N10(*registro)


        
    def infoMutua(self,registro):
        ''' Calcula la informacion mutua entre dos terminos
        
        Args:
            registro: tupla (termino1,termino2)
        
        Return:
            Probabilida: infoMutua
        '''
        try:
            self.__getDatos(registro)
            return (self.__N11/self.__N)*log((self.__N*self.__N11)/(self.__N1_*self.__N_1),2)+(self.__N01/self.__N)*log((self.__N*self.__N01)/(self.__N0_*self.__N_1),2)+(self.__N10/self.__N)*log((self.__N*self.__N10)/(self.__N1_*self.__N_0),2)+(self.__N00/self.__N)*log((self.__N*self.__N00)/(self.__N0_*self.__N_0),2)
        except:
            return 0
    
    def infMutua(self):
        ''' Calcula la informacion mutua entre una lista de terminos 
        
        Arg:
            None
            
        Return:
            None
        '''
        
        for work in self.__raices.bestRankword():
            for work1 in self.__raices.bestRankword():
                IF = self.infoMutua((work[0],work1[0]))
                if IF > 0.1:
                    self.__raices.GuardarInfoMutua((work[0],work1[0],IF))