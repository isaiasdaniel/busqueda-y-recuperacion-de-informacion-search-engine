'''
Created on 1 Jun 2012

@author: isaias
'''
from DataBase import dbCarpetas
from Indices import Indexador
import os
import Config

class Explorador(object):
    '''Este explorador cuenta con dos hilos de ejecucion
    
    1 Explora el directorio en busca de archivo nuevo o que tienen un cambio en su fecha de 
    modificacion  en cuanto encuentra esto 
    procede a analizar y gardar dentro de la base de datos con diferentes instancias del
    DBManager.
    
    2 Obtiene todo los archivo guardados dentro de la base de datos y analiza cuales todavia 
    existen el archivo que no existe en el sistema de archivos es eliminado de la base de
    datos
    
    Caso de Uso:
                    RFI-01
                    RFI-02
                    RFI_03
    
    '''

    def __init__(self,path='/usr/share/doc/'):
        '''Constructor
        '''
        self.__pathRoot = path
        self.__formatosValidos = Config.FMTFILE        
        self.__db = dbCarpetas.dbCarpetas()
        self.__index = Indexador.Indexador()

    def Explorar(self,path=''):
        ''' Explorador de carpetas
        
        Args:
            path: Direccion raiz desde donde empezara a explorar el archivo hasto os mas profundo de 
                esta seccion del arbol de archivos
                
        Return:
            None:
        '''
        try:
            metadatosCarpetaDB = self.__db.ExisteRegistro(os.path.join(path))
            metadatosCarpetaSys = os.stat(os.path.join(self.__pathRoot,path))
            if not metadatosCarpetaDB:
                self.__db.Guardar((path,metadatosCarpetaSys.st_size,metadatosCarpetaSys.st_atime,metadatosCarpetaSys.st_mtime))        
            carpetas,archivos = self.GetContenido(path)
            for mensaje,archivo in archivos:
                metadatoArchivoSys = os.stat(os.path.join(self.__pathRoot,path,archivo))
                idArchivo = self.__db.GuardarArchivo((path,archivo,metadatoArchivoSys.st_size,metadatoArchivoSys.st_atime,metadatoArchivoSys.st_mtime))
                if mensaje=='nuevo':
                    self.__index.CrearIndice(idArchivo,self.__pathRoot,path,archivo)
                elif mensaje=='actualizar':
                    self.__index.ActualizarIndice(idArchivo,self.__pathRoot,path,archivo)            
            for carpeta in carpetas:
                self.Explorar(os.path.join(path,carpeta))
        except IOError:
            return None
        except OSError:
            return None
        except:
            pass
        
    def GetContenido(self,path):
        '''Extrae el contenido de una carpeta y los separa en carpetas y archivo
        colocandolos dentro de una listas junto con un mensaje para el indexado
        
        Args:
            path: Direccion de carperta 
            
        Return
            Tupla (carpetas,archivos)
        '''
        carpetas = []
        archivos = []        
        for contenido in os.listdir(os.path.join(self.__pathRoot,path)):      
            if os.path.isdir(os.path.join(self.__pathRoot,path,contenido)):
                carpetas.append(contenido)                
            elif os.path.isfile(os.path.join(self.__pathRoot,path,contenido)):
                mensaje = self.FiltroArchivo(path,contenido)
                if mensaje:
                    archivos.append((mensaje,contenido))        
        return (carpetas,archivos)
                    
                
    def FiltroArchivo(self,path,archivo):
        ''' Filtro de archivo si el archivo no es un formato valido regresa false
        si es un formato valido regresa "nuevo" si el archivo no existia en la base de datos
        si exite en la vase de datos pero se detecta un cambio regresa "actualizar"
        si no hay un cambio regresa false 
        
        Args: 
            path: Direccion del Archivo a Filtrar
            
        Return:
            false: si no es un formato valido de indexado
        '''
        n,formato = os.path.splitext(archivo)
        if formato in self.__formatosValidos:
            metadatosArchivoDB = self.__db.ExiteArchivo((path,archivo))
            if metadatosArchivoDB:
                if round(metadatosArchivoDB[6],2) != round(os.stat(os.path.join(self.__pathRoot,path,archivo)).st_mtime,2):
                    return 'actualizar'
            else:
                return 'nuevo'
        return False
    
    def ExplorarEliminar(self):
        '''Explora y encuentra si un archivo o una carpeta ya no existe en 
        maquina. Elimina de la base de datos si danar la integridad de las
        raices
        
        Args:
            None
            
        Returns:
            None
        '''        
        ff = self.__db.GetCarpetasArchivos_ALL()
        for carpeta,archivo in ff:
            if not os.path.exists(os.path.join(self.__pathRoot,carpeta,archivo)):
                print "-> Eliminando Archivo %s en %s"%(archivo,carpeta)
                self.__db.EliminaArchivo((carpeta,archivo))

        cc = self.__db.GETCarpetas_ALL()
        for carpeta, in cc:
            if not os.path.exists(os.path.join(self.__pathRoot,carpeta)):
                print "-> Eliminando carpeta %s"%(carpeta)
                self.__db.Eliminar(carpeta)        
        
        
        