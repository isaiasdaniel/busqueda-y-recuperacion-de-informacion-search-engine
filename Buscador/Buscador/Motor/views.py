# Create your views here.
import os, sys
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext

from Busqueda import Respuesta


def main(request):
	return render_to_response('index.html', locals(), context_instance=RequestContext(request))
	
def busqueda(request):
	try:
		r = Respuesta.Respuesta()
		res = r.ConstruirHTML(request.POST['Busqueda'],0)
		res['Busqueda'] = request.POST['Busqueda']
		return render_to_response('ask.html', res, context_instance=RequestContext(request))
	except:
		return render_to_response('error.html', locals(), context_instance=RequestContext(request))
		
def masResultados(request):
	try:
		r = Respuesta.Respuesta()
		res = r.ConstruirHTML(request.POST['Busqueda'],int(request.POST['indice']))
		res['Busqueda'] = request.POST['Busqueda']
		return render_to_response('askajax.html', res, context_instance=RequestContext(request))
	except:
		return HttpResponse('<h1 id="fin">No hay mas resultados !!!</h1>')
	
