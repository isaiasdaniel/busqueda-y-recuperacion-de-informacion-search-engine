/*
 * matrizGen.c
 *
 *  Created on: Nov 7, 2012
 *      Author: isaias
 */

#include"libLevenshtein.h"


static int distance(const char * word1, int len1, const char * word2, int len2){
    int matrix[len1 + 1][len2 + 1];
    int i;
    for (i = 0; i <= len1; i++) {
        matrix[i][0] = i;
    }
    for (i = 0; i <= len2; i++) {
        matrix[0][i] = i;
    }
    for (i = 1; i <= len1; i++) {
        int j;
        char c1;

        c1 = word1[i-1];
        for (j = 1; j <= len2; j++) {
            char c2;

            c2 = word2[j-1];
            if (c1 == c2) {
                matrix[i][j] = matrix[i-1][j-1];
            }
            else {
                int delete;
                int insert;
                int substitute;
                int minimum;

                delete = matrix[i-1][j] + 1;
                insert = matrix[i][j-1] + 1;
                substitute = matrix[i-1][j-1] + 1;
                minimum = delete;
                if (insert < minimum) {
                    minimum = insert;
                }
                if (substitute < minimum) {
                    minimum = substitute;
                }
                matrix[i][j] = minimum;
            }
        }
    }
    return matrix[len1][len2];
}

static int estaPalabra(char *Palabra, PyObject *Lista){
	int i=0;
	char *Palabra2 = NULL;
	for (i=0; i<PyList_Size(Lista); i++){
		Palabra2 = PyString_AS_STRING(PyTuple_GetItem(PyList_GetItem(Lista,i),0));
		if(!strcmp(Palabra,Palabra2))
			return 1;
	}
	return 0;
}

static PyObject *IterLista(char *p, PyObject *ListPalabras){
	PyObject *NewList = PyList_New(0);
	int i;
	char *Palabra2 = NULL;
	for(i=0; i<PyList_Size(ListPalabras);i++){
		Palabra2 = PyString_AS_STRING(PyTuple_GetItem(PyList_GetItem(ListPalabras,i),0));
		PyList_Append(NewList,Py_BuildValue("(i,s)",distance(p,strlen(p),Palabra2,strlen(Palabra2)),Palabra2));

	}
	return NewList;
}

static PyObject *Distance(PyObject *self, PyObject *args){
	PyObject *Lista = NULL;
	char *Palabra = NULL;
	if (!PyArg_ParseTuple(args, "Os", &Lista, &Palabra))
	    return NULL;
	if(PyList_Check(Lista))
		if (!estaPalabra(Palabra,Lista)){
			return IterLista(Palabra,Lista);
		}
	return PyList_New(0);
}

static PyMethodDef LevenshteinMethods[] = {
    {"dist", Distance, METH_VARARGS,"Encuentra la palabra mas cercana a otra palabra de entre una lista"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initLevenshtein(void)
{
    (void) Py_InitModule("Levenshtein", LevenshteinMethods);
}
