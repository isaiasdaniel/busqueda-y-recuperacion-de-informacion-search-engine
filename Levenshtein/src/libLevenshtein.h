/*
 * similitudn.h
 *
 *  Created on: Nov 7, 2012
 *      Author: isaias
 */

#ifndef SIMILITUD_H_
#define SIMILITUD_H_

#include<Python.h>

static PyObject *Distance(PyObject *, PyObject *);
static int distance(const char *, int , const char *, int );
static int estaPalabra(char *, PyObject *);
static PyObject *IterLista(char *, PyObject *);

#endif /* SIMILITUD_H_ */
