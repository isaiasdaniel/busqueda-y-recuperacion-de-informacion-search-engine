/*
 * matrizGen.c
 *
 *  Created on: Nov 7, 2012
 *      Author: isaias
 */

#include"libsimilitud.h"

double magnitud(PyObject *Vector){
	int i = 0;
	double mag = 0.0;
	for(i=0 ; i<PyTuple_Size(Vector); i++){
		mag += pow(PyFloat_AsDouble(PyTuple_GetItem(Vector,i)),2.0);
	}
	return sqrt(mag);
}

static PyObject *anguloCos(PyObject *Consulta, PyObject *Vector){
	double sum=0.0, a=0.0, b=0.0;
	int i=0;
	for (i=0; i<PyTuple_Size(Consulta); i++){
		a = PyFloat_AsDouble(PyTuple_GetItem(Consulta,i));
		b = PyFloat_AsDouble(PyTuple_GetItem(Vector,i));
		sum += (a*b);
	}
	return Py_BuildValue("d",fabs(sum/(magnitud(Consulta)*magnitud(Vector))));


}

static PyObject *MedidasSimilitud(PyObject *Consulta, PyObject *Vectores){
	PyObject *Similitudes = PyDict_New();
	PyObject *Archivos = PyDict_Keys(Vectores);
	PyObject *keyArchivo = NULL;
	PyObject *vect = NULL;
	int i=0;
	for (i=0; i<PyList_Size(Archivos);i++){
		keyArchivo = PyList_GetItem(Archivos,i);
		vect = PyDict_GetItem(Vectores,keyArchivo);
		PyDict_SetItem(Similitudes,keyArchivo,anguloCos(Consulta,vect));
	}
	return Similitudes;
}

static PyObject *Cos(PyObject *self, PyObject *args){

	PyObject *Vectores = NULL;
	PyObject *Consulta = NULL;
	if (!PyArg_ParseTuple(args, "OO", &Consulta, &Vectores))
		return NULL;

	if(PyTuple_Check(Consulta) && PyDict_Check(Vectores)){
		return MedidasSimilitud(Consulta,Vectores);

	}

	return NULL;
}


static PyMethodDef similitudMethods[] = {
    {"cos", Cos, METH_VARARGS,"Calcula la similitud entre los vectores de los indices y el indice de busqueda"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initsimilitud(void)
{
    (void) Py_InitModule("similitud", similitudMethods);
}
