/*
 * similitudn.h
 *
 *  Created on: Nov 7, 2012
 *      Author: isaias
 */

#ifndef SIMILITUD_H_
#define SIMILITUD_H_

#include<Python.h>

static PyObject *Cos(PyObject *, PyObject *);
static PyObject *MedidasSimilitud(PyObject *, PyObject *);
double magnitud(PyObject *);
static PyObject *anguloCos(PyObject *, PyObject *);
#endif /* SIMILITUD_H_ */
