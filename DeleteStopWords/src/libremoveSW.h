/*
 * DeleteStopWords.h
 *
 *  Created on: 22/09/2012
 *      Author: daniel
 */

#ifndef DELETESTOPWORDS_H_
#define DELETESTOPWORDS_H_

#include<Python.h>


static PyObject *remove_StopWords(PyObject *, PyObject *);
PyObject * Delete(PyObject *, PyObject *);


#endif /* DELETESTOPWORDS_H_ */
