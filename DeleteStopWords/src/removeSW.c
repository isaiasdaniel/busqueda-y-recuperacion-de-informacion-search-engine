#include"libremoveSW.h"

PyObject *Delete(PyObject *sw, PyObject *txt){
	int i,j,flag;
	PyObject *listaSW = NULL;
	PyObject *listaNew = PyList_New(0);
	char *str1=NULL,*str2=NULL;

	for (i = 0; i < PyList_Size(txt); i++){
		str1=PyString_AsString(PyList_GetItem(txt,i));
		if((strcmp(str1,""))&&isalpha(str1[0])){
			char key = str1[0];
			listaSW = PyDict_GetItem(sw,Py_BuildValue("c",key));
			if (PyList_Check(listaSW)==1){
				for (j = 0; j < PyList_Size(listaSW); j++){
					str2=PyString_AsString(PyList_GetItem(listaSW,j));
					if (!strcmp(str1,str2)){
						flag=1;
						break;
					}
				}
				if(flag==1){
					flag=0;
				}else{
					PyList_Append(listaNew,Py_BuildValue("s",str1));
				}
			}

		}
	}
	return listaNew;
}

static PyObject *remove_StopWords(PyObject *self, PyObject *args) {
    PyObject *stopwords = NULL;
    PyObject *text = NULL;
    if (!PyArg_ParseTuple(args, "OO", &stopwords, &text))
        return NULL;

    if((PyDict_Check(stopwords) == 1) && (PyList_Check(text) == 1)){
    	return Delete(stopwords,text);}

    return Py_BuildValue("i", 0);
}

static PyMethodDef removeSWMethods[] = {
    {"remove",  remove_StopWords, METH_VARARGS,"Execute a shell command."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initremoveSW(void)
{
    (void) Py_InitModule("removeSW", removeSWMethods);
}
