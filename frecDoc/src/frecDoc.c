/*
 * listStemmer.c

 *
 *  Created on: 03/10/2012
 *      Author: daniel
 */

#include"libfrecDoc.h"

static PyObject *contTerm(PyObject *self, PyObject *args) {
    PyObject *list = NULL;
    PyObject *dict = PyDict_New();
    int i;
    float max=1.0;
    PyObject *value;
    PyObject *str1;
    if (!PyArg_ParseTuple(args, "O", &list))
        return NULL;

    if(PyList_Check(list) == 1){
    	for (i = 0; i < PyList_Size(list); i++){
    			str1 = PyList_GetItem(list,i);
    			value = PyDict_GetItem(dict,str1);
    			if(value){
    				float valueNow = PyFloat_AsDouble(value)+1.0;
    				if (valueNow>max){ max=valueNow;}
    				PyObject *num =  Py_BuildValue("f",valueNow);
    				PyDict_SetItem(dict,str1,num);
    			}else{
     				PyObject *num =  Py_BuildValue("f",1.0);
    				PyDict_SetItem(dict,str1,num);
    			}
    	}
    }
    normaliza(dict,max);
    return dict;
}


static void normaliza(PyObject *dict, float max){
	PyObject *list = PyDict_Keys(dict);
	int i;
	PyObject *value;
    PyObject *str1;
	for (i = 0; i < PyList_Size(list); i++){
		str1 = PyList_GetItem(list,i);
		value = PyDict_GetItem(dict,str1);
		double valueNew = PyFloat_AsDouble(value)/max;

		PyObject *num =  Py_BuildValue("f",valueNew);
		PyDict_SetItem(dict,str1,num);
	}
}

static PyMethodDef frecDocMethods[] = {
    {"fd", contTerm, METH_VARARGS,"Execute a shell command."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initfrecDoc(void)
{
    (void) Py_InitModule("frecDoc", frecDocMethods);
}


