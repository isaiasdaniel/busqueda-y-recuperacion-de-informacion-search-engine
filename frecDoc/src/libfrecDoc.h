/*
 * listStemmer.h
 *
 *  Created on: 03/10/2012
 *      Author: daniel
 */

#ifndef FRECDOC_H_
#define FRECDOC_H_

#include<Python.h>

static PyObject *contTerm(PyObject *, PyObject *);
static void normaliza(PyObject *, float);

#endif /* FRECDOC_H_ */
