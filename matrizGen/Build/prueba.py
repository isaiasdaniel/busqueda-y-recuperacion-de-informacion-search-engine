#! /usr/bin/env python

import matrizGen
import psycopg2
import scipy
from scipy import linalg

if __name__=='__main__':
	conn = psycopg2.connect("dbname=indices user=isaias")
	cur =  conn.cursor()
	cur.execute("SELECT  SQ_Arc.archivo_id,  SQ_Rai.raiz,  SQ_Rai.tf_idf FROM (SELECT archivos.archivo_id,  count(*) as num FROM     archivos,     terminos,     raices  WHERE     archivos.archivo_id = terminos.archivo_id   AND raices.idf<>0  AND terminos.raiz_id = raices.raiz_id    AND raices.raiz in ('html','web','list','python') GROUP BY  archivos.archivo_id ORDER BY  num desc LIMIT 5) SQ_Arc  INNER JOIN  (SELECT     archivos.archivo_id,    raices.raiz,    terminos.ft*log((SELECT count(*) FROM Archivos)/raices.idf) as tf_idf FROM   archivos,     terminos,     raices WHERE     archivos.archivo_id = terminos.archivo_id  AND raices.idf <> 0   AND  terminos.raiz_id = raices.raiz_id) SQ_Rai  ON  SQ_Arc.archivo_id = SQ_Rai.archivo_id ORDER BY  SQ_Rai.raiz ;")
	lista = cur.fetchall() 

	#lista = [(1,'term _1',0.00434),	(1,'term _2',0.01532),	(1,'term _3',0.12353),	(2,'term _1',0.6436),(2,'term _0',0.2432),(3,'term _3',0.2343),(3,'term _1',0.2343)]
	datos,matriz = matrizGen.mtz(lista)

	mtz = scipy.array(matriz)
	#mtz = scipy.transpose(mtz)
	u,e,v = linalg.svd(matriz,full_matrices=False)
	M,N = mtz.shape
	e = linalg.diag(e)
	
	k = int(linalg.norm(mtz,'fro'))

	scipy.dot(u,scipy.dot(e,v)).tolist()
	matriz = scipy.dot(u,scipy.dot(e,v)).tolist()
	a = matrizGen.SetMatrix(datos,matriz)

	Vect = matrizGen.GetVectores(a,['html','web'])
	print Vect
