/*
 * matrizGen.c
 *
 *  Created on: Nov 7, 2012
 *      Author: isaias
 */

#include"libmatrizGen.h"

static PyObject *GetMatriz(PyObject *archivo){
	int size = PyDict_Size(archivo);
	int i=0;
	PyObject *Matriz = PyList_New(size);
	PyObject *keys = PyDict_Keys(archivo);
	for(i=0;i<size;i++){
		PyObject *k = PyList_GetItem(keys,i);
		PyObject *values =  PyDict_Values(PyDict_GetItem(archivo,k));
		PyList_SetItem(Matriz,i,values);
	}
	return Matriz;
}

void SetValor(PyObject *archivos,PyObject *dato){
	PyObject *idArchivo = PyTuple_GetItem(dato,0);
	PyObject *raiz = PyTuple_GetItem(dato,1);
	PyObject *valor = PyTuple_GetItem(dato,2);
	PyObject *terminos = PyDict_GetItem(archivos,idArchivo);
	PyDict_SetItem(terminos,raiz,valor);
}

static PyObject *GetTerminos(PyObject * Datos){
	PyObject *terminos = PyDict_New();
	PyObject *dato = NULL;
	PyObject *palabra = NULL;
	int i;
	for (i = 0; i < PyList_Size(Datos); i++){
		dato = PyList_GetItem(Datos,i);
		palabra = PyTuple_GetItem(dato,1);
		if (PyDict_Contains(terminos,palabra)==0)
			PyDict_SetItem(terminos,palabra,Py_BuildValue("d",0.0));
	}
	return terminos;
}

static PyObject *GetArchivos(PyObject *Datos){
	PyObject *archivos = PyDict_New();
	PyObject *dato = NULL;
	PyObject *idArchivo = NULL;
	PyObject *Terminos = GetTerminos(Datos);
	int i;
	for (i = 0; i < PyList_Size(Datos); i++){
		dato = PyList_GetItem(Datos,i);
		idArchivo = PyTuple_GetItem(dato,0);
		if (PyDict_Contains(archivos,idArchivo)==0)
			PyDict_SetItem(archivos,idArchivo,PyDict_Copy(Terminos));
		SetValor(archivos,dato);
	}
	PyObject *Return = PyTuple_New(2);
	PyTuple_SetItem(Return,0,archivos);
	PyTuple_SetItem(Return,1,GetMatriz(archivos));
	return Return;
}

static PyObject *Matriz(PyObject *self, PyObject *args){
	PyObject *list = NULL;
	if (!PyArg_ParseTuple(args, "O", &list))
	        return NULL;
	if (PyList_Check(list)){
		return GetArchivos(list);
	}
	return NULL;
}

void SetData(PyObject *indices, PyObject *datos){
	int i=0,j=0;
	PyObject *Archivos = PyDict_Keys(indices);
	for(i=0;i<PyList_Size(Archivos);i++){
		PyObject *keyArchivo = PyList_GetItem(Archivos,i);
		PyObject *DictTerminos = PyDict_GetItem(indices,keyArchivo);
		PyObject *Terminos = PyDict_Keys(DictTerminos);
		for(j=0;j<PyDict_Size(DictTerminos);j++){
			PyObject *keyTermino = PyList_GetItem(Terminos,j);
			PyDict_SetItem(DictTerminos,keyTermino,PyList_GetItem(PyList_GetItem(datos,i),j));
		}
	}
}

static PyObject *SetMat(PyObject *self, PyObject *args){
	PyObject *Indices = NULL;
	PyObject *Datos = NULL;
	if (!PyArg_ParseTuple(args, "OO", &Indices, &Datos))
		return NULL;
	if (PyList_Check(Datos)&&PyDict_Check(Indices)){
		SetData(Indices,Datos);
		return Indices;
	}
	return NULL;
}

static PyObject *SetVectores(PyObject *indice, PyObject *datos){

	PyObject *Vectores = PyDict_New();
	PyObject *Archivos = PyDict_Keys(indice);
	int i=0 ,j=0;
	int tam = PyList_Size(datos);
	for(i=0; i<PyList_Size(Archivos); i++){
		PyObject *Vect = PyTuple_New(tam);
		PyObject *keyArchivo = PyList_GetItem(Archivos,i);
		PyObject *archivo = PyDict_GetItem(indice,keyArchivo);
		for(j=0;j<tam;j++){
			PyObject *valor = PyDict_GetItem(archivo,PyList_GetItem(datos,j));
			if (valor==NULL)
				PyTuple_SetItem(Vect,j,Py_BuildValue("d",0.0));
			else
				PyTuple_SetItem(Vect,j,valor);
		}
		PyDict_SetItem(Vectores,keyArchivo,Vect);
	}
	return Vectores;
}

static PyObject *GetVect(PyObject *self, PyObject *args){
	PyObject *Indices = NULL;
	PyObject *Datos = NULL;

	if (!PyArg_ParseTuple(args, "OO", &Indices, &Datos))
		return NULL;
	if (PyList_Check(Datos)&&PyDict_Check(Indices)){
		return SetVectores(Indices,Datos);

	}
	return NULL;
}

static PyMethodDef matrizGenMethods[] = {
    {"mtz", Matriz, METH_VARARGS,"Crea una matriz de Datos para realizar LSA"},
    {"SetMatrix", SetMat, METH_VARARGS,"Agrega datos con LSA una matriz"},
    {"GetVectores", GetVect, METH_VARARGS,"De los datos obtiene los vectores"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
initmatrizGen(void)
{
    (void) Py_InitModule("matrizGen", matrizGenMethods);
}
