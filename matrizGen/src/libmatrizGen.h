/*
 * matrizGen.h
 *
 *  Created on: Nov 7, 2012
 *      Author: isaias
 */

#ifndef MATRIZGEN_H_
#define MATRIZGEN_H_

#include<Python.h>

static PyObject *Matriz(PyObject *, PyObject *);
static PyObject *GetArchivos(PyObject *);
static PyObject *GetTerminos(PyObject *);
static PyObject *GetMatriz(PyObject *);
static PyObject *SetMat(PyObject *, PyObject *);
static PyObject *GetVect(PyObject *, PyObject *);
static PyObject *SetVectores(PyObject *, PyObject *);
void SetData(PyObject *, PyObject *);
void SetValor(PyObject *,PyObject *);



#endif /* MATRIZGEN_H_ */
