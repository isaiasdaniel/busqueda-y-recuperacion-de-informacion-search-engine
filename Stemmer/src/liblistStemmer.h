/*
 * listStemmer.h
 *
 *  Created on: 03/10/2012
 *      Author: daniel
 */

#ifndef LISTSTEMMER_H_
#define LISTSTEMMER_H_

#include<Python.h>

static int cons(int);
static int m(void);
static int vowelinstem(void);
static int doublec(int );
static int cvc(int );
static int ends(char * );
static void setto(char * );
static void r(char * );
static void step1ab(void);
static void step1c(void);
static void step2(void);
static void step3(void);
static void step4(void);
static void step5(void);
int stem(char * p, int i, int j);
static PyObject *StemmingList(PyObject *, PyObject *);

#endif /* LISTSTEMMER_H_ */
