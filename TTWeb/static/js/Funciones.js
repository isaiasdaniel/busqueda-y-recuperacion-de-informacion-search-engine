var doc = $(document);
doc.ready(startApp);

var i = 0;

function startApp(){
  	$("input[type=submit]").button();
   	$("#btnMas").button();
}


function NuevaBusqueda(busqueda){
	$.post('/search/',{
		Busqueda: busqueda,
		csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
	},
	function(data){
		$( "#Resultado" ).empty().append($( data ).find("#Resultado"));
		$('input[name=Busqueda]').val(busqueda)
	});
	
}

function masResultados(mas){
	i +=10;
	$("#btnMas").text("Buscando...")
	$.post('/search_mas/',{
		Busqueda: mas,
		indice: i,
		csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
	},
	function(data){
		$( "#Resultado" ).append(data);
		$("#btnMas").text("Mas Resultados")
	});
}

